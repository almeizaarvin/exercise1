from custom_math import add, sub, mul

assert(add(1,1)==2)
assert(add(2,1)==3)
assert(add(1,0)==1)
assert(add(2,2)==4)